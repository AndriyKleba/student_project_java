package edu.practice.studentorder.validator;

import edu.practice.studentorder.domain.CityRegisterCheckerResponse;
import edu.practice.studentorder.domain.Person;
import edu.practice.studentorder.exception.CityRegisterException;

public interface CityRegisterChecker {

    CityRegisterCheckerResponse checkPerson(Person person) throws CityRegisterException;
}

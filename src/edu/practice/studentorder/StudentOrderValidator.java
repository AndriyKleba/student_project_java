package edu.practice.studentorder;

import edu.practice.studentorder.domain.*;
import edu.practice.studentorder.mail.MailSender;
import edu.practice.studentorder.validator.ChildrenValidator;
import edu.practice.studentorder.validator.CityRegisterValidator;
import edu.practice.studentorder.validator.StudentValidator;
import edu.practice.studentorder.validator.WeddingValidator;

import java.util.LinkedList;
import java.util.List;

public class StudentOrderValidator {

    private CityRegisterValidator cityRegisterValidator;
    private WeddingValidator weddingValidator;
    private ChildrenValidator childrenValidator;
    private StudentValidator studentValidator;
    private MailSender mailSender;

    public StudentOrderValidator() {
        cityRegisterValidator = new CityRegisterValidator();
        weddingValidator = new WeddingValidator();
        studentValidator = new StudentValidator();
        childrenValidator = new ChildrenValidator();
        mailSender = new MailSender();
    }

    public static void main(String[] args) {
        StudentOrderValidator studentOrderValidator = new StudentOrderValidator();

        studentOrderValidator.checkAll();
    }

    public void checkAll() {
        List<StudentOrder> soLIst = readStudentOrders();

        for (StudentOrder so : soLIst) {
            System.out.println();
            checkOneOrder(so);
        }

    }

    public void checkOneOrder(StudentOrder so) {
        AnswerCityRegister cityAnswer = checkCityRegister(so);

        AnswerChildren childAnswer = checkChildren(so);
        AnswerWedding wedAnswer = checkIsWedding(so);
        AnswerStudent studentAnswer = checkStudent(so);

        sendMail(so);

    }


    public List<StudentOrder> readStudentOrders() {
        List<StudentOrder> soList = new LinkedList<>();

        for (int i = 0; i < soList.size(); i++) {
            StudentOrder so = SaveStudentOrder.buildStudentOrder(i);
            soList.add(so);
        }

        return soList;
    }

    public AnswerCityRegister checkCityRegister(StudentOrder so) {
        return cityRegisterValidator.checkCityRegister(so);
    }

    public AnswerWedding checkIsWedding(StudentOrder so) {
        return weddingValidator.checkIsWedding(so);
    }

    public AnswerChildren checkChildren(StudentOrder so) {
        return childrenValidator.checkChildren(so);
    }

    public AnswerStudent checkStudent(StudentOrder so) {
        return studentValidator.checkStudent(so);
    }

    public void sendMail(StudentOrder so) {
        mailSender.sendMail(so);
    }
}

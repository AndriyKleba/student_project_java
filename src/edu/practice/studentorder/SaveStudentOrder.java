package edu.practice.studentorder;

import edu.practice.studentorder.domain.Address;
import edu.practice.studentorder.domain.Adult;
import edu.practice.studentorder.domain.Child;
import edu.practice.studentorder.domain.StudentOrder;

import java.time.LocalDate;

public class SaveStudentOrder {
    public static void main(String[] args) {
        StudentOrder s = buildStudentOrder(10);
//        StudentOrder so = new StudentOrder();
//        long ans = saveStudentOrder(so);
//        System.out.println(ans);
    }

    static long saveStudentOrder(StudentOrder studentOrder) {
        long answer = 199;
        System.out.println("saveStudentOrder");

        return answer;
    }

    public static StudentOrder buildStudentOrder(long id) {
        StudentOrder so = new StudentOrder();
        so.setStudentOrderId(id);
        so.setMarriageCertificateId("" + (123456000 + id));
        so.setMarriageDate(LocalDate.of(2016, 7, 4));
        so.setMarriageOffice("Віділ ЗАГС");

        Address address = new Address("195000", "Морська", "12", "", "142");

        // чоловік
        Adult husband = new Adult("Кулібін", "Віктор", "Сергійович", LocalDate.of(1997, 8, 24));
        husband.setPassportSerial("" + (1000 + id));
        husband.setPassportNumber("" + (100000 + id));
        husband.setIssueDate(LocalDate.of(2017, 9, 15));
        husband.setIssueDepartment("Віділ міліції №" + id);
        husband.setStudentId("" + (100000 + id));
        husband.setAddress(address);
        // жінка
        Adult wife = new Adult("Кулібіна", "Вероніка", "Олексіївна", LocalDate.of(1998, 3, 12));
        wife.setPassportSerial("" + (2000 + id));
        wife.setPassportNumber("" + (200000 + id));
        wife.setIssueDate(LocalDate.of(2018, 4, 5));
        wife.setIssueDepartment("Віділ міліції №" + id);
        wife.setStudentId("" + (200000 + id));
        wife.setAddress(address);
        // дитина
        Child child = new Child("Кулібіна", "Марина", "Вікторівна", LocalDate.of(2018, 6, 29));
        child.setCertificateNumber("" + (300000 + id));
        child.setIssueDate(LocalDate.of(2018, 7, 19));
        child.setIssueDepartment("Віділ ЗАГС №" + id);
        child.setAddress(address);
//        дитина1
        Child child1 = new Child("Кулібін", "Максим", "Вікторович", LocalDate.of(2018, 6, 29));
        child1.setCertificateNumber("" + (400000 + id));
        child1.setIssueDate(LocalDate.of(2018, 7, 19));
        child1.setIssueDepartment("Віділ ЗАГС №" + id);
        child1.setAddress(address);

        so.setHusband(husband);
        so.setWife(wife);
        so.addChild(child);
        so.addChild(child1);

        return so;
    }
}

package edu.practice.studentorder.exception;

public class CityRegisterException extends RuntimeException {
    public CityRegisterException() {
    }

    public CityRegisterException(String message) {
        super(message);
    }

    public CityRegisterException(String message, Throwable cause) {
        super(message, cause);
    }
}
